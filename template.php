<?php

function phptemplate_preprocess_node(&$vars) {
  $vars['storylink_url'] = check_url($vars['node']->vote_storylink_url);
  $vars['vote_storylink_via'] = theme('vote_storylink_via', $vars['node']->vote_storylink_url);
  if (arg(1) == 'top') {
    static $count;
    $count = is_array($count) ? $count : array();
    $count[$hook] = is_int($count[$hook]) ? $count[$hook] : 1;
    $vars['seqid'] = $count[$hook]++;
  }
}